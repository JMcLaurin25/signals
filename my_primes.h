
#ifndef MY_PRIMES_H
#define MY_PRIMES_H

#include <stdbool.h>

void seive_primes(size_t limit, size_t primes[]);
bool is_prime(long int);
void fwd_prime_out(size_t base, size_t limit, size_t primes[]);
void rev_prime_out(size_t limit, size_t primes[]);

#endif
