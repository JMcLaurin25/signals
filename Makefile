
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

signaler: signaler.o handler.o my_primes.o -lm 

.PHONY: clean debug profile

clean:
	-rm *.o *.su signaler

debug: CFLAGS+=-g
debug: signaler

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: signaler

