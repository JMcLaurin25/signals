#ifndef HANDLER_C
#define HANDLER_C

struct vars {
	long int num;
	int skip;
	int direction;
};

extern struct vars *cur_vars;
void handler(int sig_num, siginfo_t *siginfo, void *data);
void vars_out(struct vars *);

#endif
