#include <stdbool.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "handler.h"
#include "my_primes.h"

struct vars *cur_vars;

static bool find_arg(const char* arg, int argc, char **argv);

int main(int argc, char **argv)
{
	char *endptr;
	long int limit = INT_MAX;
	int arg_option = 0;
	
	// Set signals to be caught
	cur_vars = malloc(sizeof(*cur_vars));
	if (!cur_vars){
		exit(1);
	}

	// Set global variables
	cur_vars->num = 1;
	cur_vars->skip = 0;
	cur_vars->direction = 1;

	// Sigaction
	struct sigaction action;
	memset(&action, 0, sizeof(action));

	action.sa_sigaction = &handler;
	action.sa_flags = SA_SIGINFO;

	/*
	 * SIGHUP: Restart emmiting prime numbers from 2
	 * SIGUSR1: Skip the next prime number
	 * SIGUSR2: Reverse direction
	 * SIGINT: Signal interrupt
	 */

	sigaction(SIGHUP, &action, NULL); 
	sigaction(SIGUSR1, &action, NULL);
	sigaction(SIGUSR2, &action, NULL);
	sigaction(SIGINT, &action, NULL);

	// Argument handling
	while ((arg_option = getopt(argc, argv, "e:r:s:")) != -1) {
		switch (arg_option) {
			case 'E':
			case 'e':
				// e <n> Exit if prime num is gt <n>
				limit = strtol(optarg, &endptr, 10);
				break;
			case 'R':
			case 'r':
				// r <n> Decreasing values from <n>
				cur_vars->num = strtol(optarg, &endptr, 10);
				cur_vars->direction = -1;

				if (find_arg("-s", argc, argv)) {
					printf("Invalid argument combination.\nCannot set a decreasing start point and Increasing start point.\n");
					goto Invalid_args;
				}

				break;
			case 'S':
			case 's':
				// s <n> Start next prime gt <n>
				cur_vars->num = strtol(optarg, &endptr, 10);

				if (cur_vars->num < 0) {
					printf("Argument value too small\n");
					goto Invalid_args;
				}
				if (find_arg("-r", argc, argv)) {
					printf("Invalid argument combination.\nCannot set a decreasing start point and Increasing start point.\n");
					goto Invalid_args;
				}

				break;
			default:
Invalid_args:
				free(cur_vars);
				exit(1);
		}
	}

	// Output Loop
	while (1){
		if (cur_vars->num < 1 || cur_vars->num > limit) {
			break;
		}

		if (is_prime(cur_vars->num)){
			if (cur_vars->skip == 1){
				//Don't print this one
				cur_vars->skip = 0;
			} else {
				printf("%ld\n", cur_vars->num);
				sleep(1);
			}
		}

		cur_vars->num += cur_vars->direction;	
	}

	free(cur_vars);
}

/*
 * find_arg() function searches an array of strings and returns true/false
 * for values existence in the array.
 */
static bool find_arg(const char* arg, int argc, char **argv)
{
	if (!arg || !*argv) {
		return false;
	}

	for (int idx = 0; idx < argc; idx++) {
		if (strncmp(argv[idx], arg, strlen(arg)) == 0) {
			return true;
		}
	}

	return false;
}
