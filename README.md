# Signaler #

Write a program that prints out increasing prime numbers to standard output, approximately one every second. It should respond to the following signals:

**SIGHUP** Restart emitting prime numbers from 2.

**SIGUSR1** Skip the next prime number to be emitted.

**SIGUSR2** Reverse direction; go from emitting increasing to decreasing, or vice versa. 

If the program has to emit a prime number less than 2, it should exit.

It should also support the following optional command-line flags:

-s <n> start at the next prime number greater than n

-r <n> emit decreasing values from number n, incompatible with -s

-e <n> exit the program if a prime number greater than n would be printed

The executable should be signaler and the repo should be signals

Program documentation to be saved as sigs_doc.pdf
