#include <stdbool.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/* Prime function derived from Sieve of Eratosthenes
 * https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
 * https://www.quora.com/What-is-the-code-for-applying-sieve-of-eratosthenes
 * -in-C-programming
 */

// Not used
void seive_primes(size_t limit, size_t primes[])
{
	if (!primes) {
		return;
	}
	size_t idx_i, idx_j;

	// Set all primes to true
	for (idx_i = 0; idx_i < limit; idx_i++) {
		primes[idx_i] = 1;
	}

	primes[0] = 0, primes[1] = 0; // First two values are not prime
	
	// Find prime values
	for (idx_i = 2; idx_i < sqrt(limit); idx_i++) {
		for (idx_j = pow(idx_i, 2); idx_j < limit; idx_j += idx_i) {
			primes[idx_j] = 0;
		}
	}
}

/*
 * is_prime() function tests a single element for primality
 * true is prime
 * false is not prime
 */
bool is_prime(long int num)
{
	if (num < 2){
		return false;
	}
	for (long int idx = 2; idx < sqrt(num) + 1; idx++){
		if (num % idx == 0 && idx != num){
			return false;
		}
	}
	return true;
}

// Not used: prints the forward prime numbers generated from seive_primes()
void fwd_prime_out(size_t base, size_t limit, size_t primes[])
{
	size_t idx_i;
	for (idx_i = base; idx_i < limit; idx_i++) {
		if (primes[idx_i] == 1) {
			printf("%lu\n", idx_i);
			sleep(1);
		}
	}
}

// Not used: prints the reverse prime numbers generated from seive_primes()
void rev_prime_out(size_t limit, size_t primes[])
{
	size_t idx_i;
	for (idx_i = limit; idx_i > 0; idx_i--) {
		if (primes[idx_i - 1] == 1) {
			printf("%lu\n", idx_i - 1);
			sleep(1);
		}
	}
}
