#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include "handler.h"

struct vars *cur_vars;

// Not Used, Outputs current state of struct var content
void vars_out(struct vars *data)
{
	if (!data){
		return;
	}
	printf(" num: %ld\nskip: %d\n dir: %d\n",
		   data->num,
		   data->skip,
		   data->direction
		   );
}

/*
 * handler() function enables manipulation of struct vars elements
 */
void handler(int sig_num, siginfo_t *siginfo, void *context)
{
	(void) context;
	(void) siginfo;

	/*
	 * SIGHUP: Restart emmiting prime numbers from 2
	 * SIGUSR1: Skip the next prime number
	 * SIGUSR2: Reverse direction
	 * SIGINT: Signal interrupt
	 */

	switch (sig_num) {
		case 1: // SIGHUP
			cur_vars->num = 1;
			break;
		case 2: // SIGINT
		case 9: // SIGKILL
			free(cur_vars);
			exit(0);
			break;
		case 10: // SIGUSR1
			cur_vars->skip = 1;
			break;
		case 12: // SIGUSR2
			cur_vars->direction *= -1;
			break;
		default:
			break;
	}
}
